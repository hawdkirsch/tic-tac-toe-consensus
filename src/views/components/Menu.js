import React from 'react';
import PropTypes from 'prop-types';
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Icon from "@material-ui/core/Icon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {NavLink} from "react-router-dom";


const Menu = ({ open, onItemClick, onClose, logout }) => {



    return (
        <Drawer open={open} onClose={onClose} anchor="top">
            <List>
                <ListItem button onClick={() => onItemClick('new')}>
                    <ListItemIcon>
                        <Icon>fiber_new</Icon>
                    </ListItemIcon>
                    <ListItemText>New game</ListItemText>
                </ListItem>
                <ListItem
                    to="/games/my"
                    component={NavLink}
                    onClick={onClose}
                >
                    <ListItemIcon>
                        <Icon>apps</Icon>
                    </ListItemIcon>
                    <ListItemText>My Games</ListItemText>
                </ListItem>
                <ListItem
                    to="/games"
                    component={NavLink}
                    onClick={onClose}
                >
                    <ListItemIcon>
                        <Icon>apps</Icon>
                    </ListItemIcon>
                    <ListItemText>Joinable Games</ListItemText>
                </ListItem>
                <ListItem button onClick={logout}>
                    <ListItemIcon>
                        <Icon>exit_to_app</Icon>
                    </ListItemIcon>
                    <ListItemText>Log out</ListItemText>
                </ListItem>
            </List>
        </Drawer>
    );
};

const { func, bool } = PropTypes;

Menu.propTypes = {
    open: bool.isRequired,
    onItemClick: func.isRequired,
    onClose: func.isRequired,
    logout: func.isRequired,
};

export default Menu;