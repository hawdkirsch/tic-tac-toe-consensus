import React, {Component} from "react";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Button from "@material-ui/core/Button";
import {getPlayerTables} from "../../state/utils/blockchain";
import PropTypes from "prop-types";
import {tableOperations, tableSelectors} from "../../state/ducks/tables";
import connect from "react-redux/es/connect/connect";
import {withStyles} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import {NavLink} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import GameState from "../../state/utils/game-state";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationBar from "./TablePaginationBar";


const styles = theme => ({
    root: {
        width: '100%',
        // maxWidth: 500,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    container: {
        display: 'inline-grid',
        width: '100%',
    },
    table: {
        minWidth: 700,
    },
});

class PlayerTable extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        page: 0,
        rowsPerPage: 10,
    };


    componentDidMount = async () => {
        const pubKey = this.props.accountState.account.pubKey;
        window.setInterval(async () => {
            const playerTables = await getPlayerTables(pubKey);
            this.props.setPlayerTables(playerTables);
        }, 2000);
    };

    handleChangePage = (event, newPage) => {
        this.setState({ page: newPage });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: Number(event.target.value) });
    };

    render() {
        const { playerTables = [] } = this.props;
        const { page, rowsPerPage } = this.state;
        return (
            <>
                <Typography variant="h5">My Games</Typography>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Table Name</TableCell>
                            <TableCell align={"right"}>State</TableCell>
                            <TableCell align="right">Whose turn</TableCell>
                            <TableCell align="right">Sign</TableCell>
                            <TableCell align="right"></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {playerTables
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map(row => (
                                <TableRow key={row.name}>
                                    <TableCell component="th" scope="row">
                                        {row.name}
                                    </TableCell>
                                    <TableCell align="right">{row.state}</TableCell>
                                    <TableCell align="right">{row.whose_turn}</TableCell>
                                    <TableCell align="right">{row.sign}</TableCell>
                                    <TableCell align="right">
                                        <Button
                                            component={NavLink}
                                            to={`/games/${row.name}`}
                                            disabled={row.state===GameState.Initial}
                                        >
                                            {[GameState.Initial, GameState.Playing].includes(row.state) ? 'PLAY' : 'VIEW'}
                                        </Button>
                                    </TableCell>
                                </TableRow>
                        ))}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[10, 15, 20]}
                                colSpan={3}
                                count={playerTables.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationBar}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </>
        )
    }
}

PlayerTable.propTypes = {
    classes: PropTypes.object.isRequired,
    setPlayerTables: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        ...state,
        playerTables: tableSelectors.getPlayerTables(state),
    };
};

const mapDispatchToProps = {
    setPlayerTables: tableOperations.setPlayerTables,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(PlayerTable));
