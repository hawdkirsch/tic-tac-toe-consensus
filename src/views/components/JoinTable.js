import React, {Component} from "react";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import {tableOperations, tableSelectors} from "../../state/ducks/tables";
import connect from "react-redux/es/connect/connect";
import {withStyles} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationBar from "./TablePaginationBar";


const styles = theme => ({
    root: {
        width: '100%',
        // maxWidth: 500,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    container: {
        display: 'inline-grid',
        width: '100%',
    },
    table: {
        minWidth: 700,
    },
});

class JoinTable extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        page: 0,
        rowsPerPage: 10,
    };

    componentDidMount = async () => {
        const { loadOpenTables } = this.props;
        window.setInterval(async () => {
            loadOpenTables()
        }, 2000);
    };

    handleChangePage = (event, newPage) => {
        this.setState({ page: newPage });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: Number(event.target.value) });
    };

    join = name => () => {
        const { joinGame } = this.props;
        
        try {
            joinGame(name);
        } catch (pc_e) {
            throw Error(pc_e);
        }
    };

    render() {
        const { openTables = [] } = this.props;
        const { page, rowsPerPage } = this.state;

        return (
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Table Name</TableCell>
                        <TableCell align="right">Opponent</TableCell>
                        <TableCell align="right">Join</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {openTables.map(row => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell align="right">{row.whose_turn}</TableCell>
                            <TableCell align="right"><Button onClick={this.join(row.name)}>JOIN</Button></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[10, 15, 20]}
                            colSpan={3}
                            count={openTables.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationBar}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        );
    }
}

const {func, object} = PropTypes;
JoinTable.propTypes = {
    classes: PropTypes.object.isRequired,
    loadOpenTables: func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        ...state,
        openTables: tableSelectors.getOpenTables(state),
    };
};

const mapDispatchToProps = {
    loadOpenTables: () => ({type: "LOAD_OPEN_TABLES"}),
    joinGame: name => ({type: "JOIN_GAME", payload: name}),
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(JoinTable));
