// views/components/Board.jsx

import React from 'react';
import PropTypes from 'prop-types';


import Square from './Square';
import Grid from "@material-ui/core/Grid";
import {withStyles} from "@material-ui/core";
import * as classnames from "classnames";
import { winningPattern } from "../../state/utils/game";

const borderStyle = '1px solid black';

// styles is a function that accepts the base theme object from material-ui
// and returns an object with either overrides or new styles to apply to
// components and elements
const styles = (theme) => ({
    root: {
        margin: 50,
        position: 'relative',
        width: 300,
        height: 300,
    },
    // a square's dimension is 100x100px
    square: {
        height: 100,
        width: 100,
        lineHeight: '100px', // this is to center the icon in the square
        fontSize: '48px', // the size of our player's icon
        cursor: 'pointer'
    },
    // if a square is marked, we show the not allowed circle with a line through it
    marked: { cursor: 'not-allowed' },
    disabled: { cursor: 'default' },
    // a rows should have its content centered
    row: { textAlign: 'center' },
    // these styles make up the border of the game cross pattern
    '0_1': { borderLeft: borderStyle, borderRight: borderStyle },
    '2_1': { borderLeft: borderStyle, borderRight: borderStyle },
    '1_1': { border: borderStyle },
    '1_0': { borderTop: borderStyle, borderBottom: borderStyle },
    '1_2': { borderTop: borderStyle, borderBottom: borderStyle },
    strikethrough: {
        backgroundColor: 'black',
        position: 'absolute',
        height: 5,
        borderRadius: 2.5,
        width: 0,
        transition: 'width 0.3s ease-out',
    },
    top: {
        left: -30,
        top: 45,
        width: 360,
    },
    middle: {
        left: -30,
        top: 145,
        width: 360,
    },
    bottom: {
        left: -30,
        top: 245,
        width: 360,
    },
    left: {
        width: 440,
        transform: 'rotate(45deg)',
        transformOrigin: 'left center',
        left: -5,
        top: -10,
    },
    central: {
        top: -30,
        left: 150,
        width: 360,
        transform: 'rotate(90deg)',
        transformOrigin: 'left center',
    },
    right: {
        width: 440,
        transform: 'rotate(-225deg)',
        transformOrigin: 'left center',
        left: 305,
        top: -10,
    },
    verticalL: {
        top: -30,
        left: 50,
        width: 360,
        transform: 'rotate(90deg)',
        transformOrigin: 'left center',
    },
    verticalR: {
        top: -30,
        left: 250,
        width: 360,
        transform: 'rotate(90deg)',
        transformOrigin: 'left center',
    },

});


const Board = ({ classes, board, onMove, sign, enabled }) => {
    // the 'board' and 'onMove' handler are passed in from the props of the Game which
    // holds this component and control the state
    // we will simply render the 'board' in its current state and call the 'onMove'
    // handler function given to us when a player clicks on a Square
    let boardLines = [];
    boardLines.push([board[0], board[1], board[2]]); // TODO improve
    boardLines.push([board[3], board[4], board[5]]);
    boardLines.push([board[6], board[7], board[8]]);

    const handleClick = square => () => {
        if (!enabled) {
            return;
        }

        onMove && onMove(square);
    };

    const strikethroughClasses = [
        classes.top, classes.middle, classes.bottom,
        classes.left, classes.right,
        classes.verticalL, classes.central, classes.verticalR
    ];

    const patternIndex = winningPattern(board)

    const strikethroughClass = patternIndex >= 0 ? strikethroughClasses[patternIndex] : '';

    return (
        <Grid container className={classes.root}>
            <div className={classnames(classes.strikethrough, strikethroughClass)} />
            {boardLines.map((row, rIdx) => (
                <Grid key={rIdx} item xs={12} className={classes.row}>
                    <Grid container justify="center">
                        {row.map((col, cIdx) => {
                            // the border style for a square as defined by the styles object above
                            const border = classes[`${rIdx}_${cIdx}`] || '';
                            // remember that 0 is for NONE in our players enum...which we should add
                            const marked = col !== 0 ? classes.marked : '';
                            const disabled = !enabled ? classes.disabled : '';

                            return (
                                <Grid
                                    key={cIdx}
                                    item
                                    className={classnames(classes.square, border, marked, disabled)}
                                    onClick={handleClick((rIdx*3) + cIdx)}>
                                    {/*
                      we have two options here, we can add the click event to the grid item or we can
                      or we can pass it down to the Square to call when it is clicked on.
                      if we pass it down, we will need to update the square to accept the onMove event
                      and give the coordinates that it resides in
                    */}
                                    <Square player={col} sign={sign}/>
                                </Grid>
                            );
                        })}
                    </Grid>
                </Grid>
            ))}
        </Grid>
    );
};

const { arrayOf, number, object, func } = PropTypes;

Board.propTypes = {
    classes: object.isRequired,
    board: arrayOf(number).isRequired,
    onMove: func.isRequired,
    sign: number
};

export default withStyles(styles)(Board);