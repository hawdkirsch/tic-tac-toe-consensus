import React  from "react";
import * as classnames from "classnames";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import State from "../../state/utils/game-state";

const styles = {
    transitionStartState: {
        fontSize: '1rem',
        color: '#0000',
    },
    animation: {
        transition: 'all 0.3s ease-out',
    },
    transitionEndState: {
        fontSize: '3rem',
        color: '#000',
    }
};

const GameState = ({ state, myName, isMyTurn, animate, winner, classes }) => {
    const draw = state === State.Draw;
    const gameover = draw || state === State.GameOver;

    return (
        <>
            {!gameover && state !== undefined && (isMyTurn ? (
                <Typography className={classnames(classes.transitionEndState)}>
                    Your turn
                </Typography>
            ) : (
                <Typography className={classnames(classes.transitionEndState)}>
                    Other player turn
                </Typography>
            ))}
            {gameover && (
                <Typography className={classnames(classes.transitionEndState)}>
                    {draw ? "Draw" : "Game Over!!!"}
                </Typography>
            )}
            {gameover && !draw && (
                <Typography className={classnames(classes.transitionEndState)}>
                    {myName === winner ? "You Won!" : "You've Lost"}
                </Typography>
            )}
        </>
    )
};

export default withStyles(styles)(GameState);