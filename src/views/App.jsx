import React from 'react';
import {Switch, Route, Redirect} from "react-router";
import PropTypes from 'prop-types';

import {withStyles} from '@material-ui/core'
import Game from "./containers/Game";
import TitleBar from "./containers/TitleBar";
import Login from "./components/Login";
import {accountOperations, accountSelectors} from "../state/ducks/account";
import connect from "react-redux/es/connect/connect";
import {tableOperations, tableSelectors} from "../state/ducks/tables";
import JoinTable from "./components/JoinTable";
import PlayerTable from "./components/PlayerTable";

const styles = (theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    content: {
        paddingTop: theme.mixins.toolbar.minHeight + 10,
        width: 700,
    }
});

const App = (state) => {
    const classes = state.classes;

    const account = state.accountState;
    const loggedIn = account.loggedIn;

    return (
        <div className={classes.root}>
            {loggedIn ? (
                <div>
                    <div>
                        <header>
                            <TitleBar/>
                        </header>
                    </div>
                    <div className={classes.content}>
                        <Switch>
                            <Route path="/games" exact component={JoinTable} />
                            <Route path="/games/my" exact component={PlayerTable} />
                            <Route path="/games/:gameId" component={Game} />
                            <Redirect to="/games/my"/>
                        </Switch>
                    </div>
                </div>
            ) : (
                <div>
                    <Switch>
                        <Route path="/" exact component={Login} />
                        <Redirect to="/"/>
                    </Switch>
                </div>
            )}
        </div>
    );
};

const {number, func} = PropTypes;

App.propTypes = {
    classes: PropTypes.object.isRequired,
    state: PropTypes.object,

    // TOREMOVE
    setName: func.isRequired,
    generatePubKey: func.isRequired,
    generateKeyPair: func.isRequired,
    login: func.isRequired,
};


const mapStateToProps = (state) => {
    return {
        ...state,
        accountState: {
            loggedIn: accountSelectors.loggedIn(state),
        },
        tablesState: {
            selectedTable: tableSelectors.getSelectedTable(state),
        }
    };
};

const mapDispatchToProps = {
    setName: accountOperations.settingName,
    generatePubKey: accountOperations.generatePub,
    generateKeyPair: accountOperations.generateKeyPair,
    login: accountOperations.login,
    selectTable: tableOperations.selectTable,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));
