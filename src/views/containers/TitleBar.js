
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


import Menu from '../components/Menu';

import { gameOperations } from '../../state/ducks/game';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/es/styles/withStyles";
import Icon from "@material-ui/core/Icon";
import {accountState} from "../../state/ducks";
import {accountOperations, accountSelectors} from "../../state/ducks/account";
import {newGame} from "../../state/utils/blockchain";

// this is from the material-ui site for centering the menu icon
const styles = () => ({
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});

class TitleBar extends Component {
    constructor(props, context) {
        super(props, context);

        // since the menu is only used here, we can use local state to
        // keep track of when it is open or closed
        this.state = { menuOpen: false };

        this.handleMenuClick = this.handleMenuClick.bind(this);
        this.handleMenuClose = this.handleMenuClose.bind(this);
        this.handleNewGameClick = this.handleNewGameClick.bind(this);
    }

    handleMenuClick() {
        this.setState({ menuOpen: true });
    }

    handleMenuClose() {
        this.setState({ menuOpen: false });
    }

    handleNewGameClick = async (itemKey) => {
        const { createGame } = this.props;

        if (itemKey === 'new') {
            try {
                createGame();
            } catch(e) {
                throw Error(e);
            }

        }

        this.setState({ menuOpen: false });
    }

    logout = () => {
        this.props.logout();
    };

    render() {
        const { classes } = this.props;
        const { menuOpen } = this.state;

        return (
            // basicly the default template sample for the title bar with a menu icon
            // from material-ui demo site
            <div>
                <AppBar>
                    <Toolbar>
                        <IconButton
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="Menu"
                            onClick={this.handleMenuClick}>
                            <Icon>menu</Icon>
                        </IconButton>
                        <Typography variant="title" color="inherit">
                            Tic Tac Toe
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Menu open={menuOpen} onItemClick={this.handleNewGameClick} onClose={this.handleMenuClose} logout={this.logout} />
            </div>
        );
    }
}

const { object, func } = PropTypes;

TitleBar.propTypes = {
    classes: object.isRequired,
    createGame: func.isRequired,
    logout: func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        ...state,
        account: accountSelectors.getAccount(state),
    };
};


const mapDispatchToProps = {
    logout: accountOperations.logout,
    createGame: () => ({type: "CREATE_GAME"}),
};

const styledTitleBar = withStyles(styles)(TitleBar);

export default connect(mapStateToProps, mapDispatchToProps)(styledTitleBar);