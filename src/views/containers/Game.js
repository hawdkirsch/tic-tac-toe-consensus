import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';

import { gameOperations } from '../../state/ducks/game';
import { accountSelectors } from '../../state/ducks/account';

import Board from '../components/Board';
import GameoverDialog from '../components/GameoverDialog';
import GameState from '../../state/utils/game-state';
import GameStateComponent from '../components/GameState';

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }
};

class Game extends Component {
    state = {
        showDialog: false
    };

    componentDidMount = async () => {
        const { match: { params: { gameId } }, loadBoardState }  = this.props;
        loadBoardState(gameId);
    };

    componentWillUnmount = () => {
        const { clearGame } = this.props;
        this.stopPeriodicSync()
        clearGame();
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { game: { isMyTurn, state } } = this.props;

        if (!isMyTurn && !this.syncHandler && state == GameState.Playing) {
            this.startPeriodicSync();
        } else if ((isMyTurn || state !== GameState.Playing) && this.syncHandler) {
            this.stopPeriodicSync();
        }

        const prevGameState = (prevProps.game || {}).state;

        if (prevGameState === GameState.Playing && (state === GameState.Draw || state === GameState.GameOver)) {
            this.setState({ showDialog: true });
        }

        if (this.state.animate === undefined && state !== undefined) {
            if (prevGameState === undefined && (state === GameState.Draw || state === GameState.GameOver)) {
                this.setState({ animate: false });
            } else {
                this.setState({ animate: true });
            }
        }
    }

    startPeriodicSync = () => {
        const { match: { params: { gameId } }, loadBoardState } = this.props;
        if (!gameId) {
            return;
        }

        this.stopPeriodicSync();

        this.syncHandler = setInterval(async () => {
            loadBoardState(gameId);

        }, 1000);
    };

    stopPeriodicSync = () => {
        if (this.syncHandler) {
            clearInterval(this.syncHandler);
        }

        this.syncHandler = null;
    };

    handleBoardOnMove = square => {
        const {gameover, makeMove, game: { mySign, board }} = this.props;

        if (gameover || board[square] != 0) {
            return;
        }

        makeMove(mySign, square)
    };

    handleDialogClick = answer => {
        // we only want to start a new game if the player clicks 'yes'
        const { createGame } = this.props;
        if (answer) {
            createGame();
        }

        // we always want to close the dialog
        this.setState({showDialog: false});
    };

    handleDialogClose = () => {
        // close the dialog
        this.setState({showDialog: false});
    };

    render() {
        const {showDialog, animate} = this.state;
        const { isBoardLocked, account: { name }, classes, game: {isMyTurn, board, state, winner } } = this.props;
        const draw = state === GameState.Draw;
        return (
            <>
                <div className={classes.container}>
                    <Board board={board} onMove={this.handleBoardOnMove} enabled={!isBoardLocked}/>
                    <GameStateComponent
                        animate={animate}
                        myName={name}
                        isMyTurn={isMyTurn}
                        state={state}
                        winner={winner}
                    />
                </div>
                <GameoverDialog
                    open={showDialog}
                    isDraw={draw}
                    player={winner}
                    onClick={this.handleDialogClick}
                    onClose={this.handleDialogClose}
                />
            </>
        );
    }
}

const {arrayOf, number, func, bool, any, object} = PropTypes;

Game.propTypes = {
    player: number.isRequired,
    gameover: bool.isRequired,
    newGame: func.isRequired,
    account: object.isRequired,
    sign: number,
};

const mapStateToProps = (state) => {
    const {gameState} = state;

    return {
        loading: gameState.loading,
        player: gameState.player,
        gameover: gameState.gameover,
        account: accountSelectors.getAccount(state),
        sign: gameState.gamePlayer.sign,
        game: gameState.game,
        isBoardLocked: gameState.isBoardLocked,
        winner: gameState.winner,
        gameState
    };
};

const mapDispatchToProps = {
    newGame: gameOperations.newGame,
    makeMove: (player, cell) => ({type: "MAKE_MOVE", payload: {player, cell}}),
    loadBoardState: name => ({type: "LOAD_BOARD_STATE", payload: name}),
    clearGame: () => ({type: "CLEAR_GAME"}),
    createGame: () => ({type: "CREATE_GAME"}),
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Game));