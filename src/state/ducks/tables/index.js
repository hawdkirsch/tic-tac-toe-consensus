import reducer from './reducers';

import * as tableOperations from './operations';
import * as tableTypes from './types';
import * as tableSelectors from './selectors';

export {
    tableOperations,
    tableSelectors,
    tableTypes
}

export default reducer;
