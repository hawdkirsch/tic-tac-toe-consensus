import * as types from './types';

const setPlayerTables = (state) => {
    return {
        type: types.ADD_PLAYER_TABLES,
        payload: state,
    };
};

const setOpenTables = (state) => {
    return {
        type: types.ADD_OPEN_TABLES,
        payload: state
    }
}

const selectTable = (state) => {
    return {
        type: types.SELECT_TABLE,
        payload: state,
    }
}

const selectTableByName = (name) => {
    return {
        type: types.SELECT_TABLE_BY_NAME,
        payload: name,
    }
}

export {
    setPlayerTables,
    setOpenTables,
    selectTable,
    selectTableByName,
}