
const getPlayerTables = (state) => {
    if (!state || !state.tablesState) return [];
    const playerTables = state.tablesState.tables.playerTables;
    return playerTables;
};

const getOpenTables = (state) => {
    if (!state || !state.tablesState) return [];
    const openTables = state.tablesState.tables.openTables;
    return openTables;
};

const getSelectedTable = (state) => {
    if(!state || !state.tablesState) return null;
    return state.tablesState.tables.selectedTable;
}

export {
    getPlayerTables,
    getOpenTables,
    getSelectedTable,
}