import * as types from './types';

const login = () => {
    return {
        type: types.LOG_IN,
    };
};

const logout = () => {
    return {
        type: types.LOG_OUT,
    }
}

const setName = (name) => {
    return {
        type: types.ADD_NAME,
        payload: name
    }
};

const generatePubKey = (privKey) => {
    return {
        type: types.GENERATE_PUBKEY,
        payload: privKey
    }
};

const generateKeyPair = () => {
    return {
        type: types.GENERATE_KEYPAIR,
    }
};

export {
    login,
    setName,
    generatePubKey,
    generateKeyPair,
    logout
}