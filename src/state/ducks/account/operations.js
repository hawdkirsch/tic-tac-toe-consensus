import { login, setName, generatePubKey, generateKeyPair, logout } from './actions';

const generatePub = (priv) => (dispatch) => {
    dispatch(generatePubKey(priv));
}

const settingName = (name) => (dispatch) => {
    dispatch(setName(name));
};

export {
    login,
    settingName,
    generatePub,
    generateKeyPair,
    logout
}