const getAccount = (state) => {
    const account = state.accountState.account;
    const name = account.name? account.name : "";
    const privKey = account.privKey? account.privKey : "";
    const pubKey = account.pubKey? account.pubKey : "";
    return {name, privKey, pubKey};
};

const loggedIn = (state) => {
    return state.accountState.account.loggedIn;
};

export {
    getAccount,
    loggedIn,
}