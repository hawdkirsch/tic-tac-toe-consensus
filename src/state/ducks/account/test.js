import reducer from './reducers';

describe("Account", () => {

    const onlyName = {account: {name: "Alice"}}
    const withPrivKey = {
        account: {
            name: "Alice",
            privKey: "b5c4516af43313bfbf095bb49c5331de0ebb48a76ae03aa7f969381207cc10a9",
        }
    };

    const loggedIn = {
        account:{
            ...withPrivKey,
            pubKey: "0262078611223116b985e2425afa7c4ad6416e2d84f529006ab3f4aa05cbcd8e3b"
        }
    };

    const loggedOut = {account: {}};
    const loggedInTest = {
        account: {
            name: expect.any(String),
            privKey: expect.any(String),
            pubKey: expect.any(String),
        }
    };

    describe("Auth", () => {
        it('Logs in without priv key', () => {
           const action = {type: "LOG_IN"};
           const result = reducer(onlyName, action);

           expect(result).toEqual(loggedInTest);
        });

        it("Logs in with privKey", () => {
            const action = {type: "LOG_IN"};
            const result = reducer(withPrivKey, action);

            expect(result).toEqual(loggedInTest);
        });

        it("Logs out", () => {
            const action = {type: "LOG_OUT"};
            const result = reducer(loggedIn, action);

            expect(result).toEqual(loggedOut);
        })
    });
});