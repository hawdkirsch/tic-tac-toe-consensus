import {combineReducers} from 'redux';

import * as types from './types';
import {getUser, makeKeyPair, verifyKeyPair} from "../../utils/blockchain";

const createAccount = () => {
    const account = makeKeyPair();
    return {
        privKey: account.privKey.toString('hex'),
        pubKey: account.pubKey.toString('hex'),
    }
};

const generatePubkey = (privKey) => {
    try {
        const account = verifyKeyPair(Buffer.from(privKey, 'hex'));
        return {
            privKey: account.privKey.toString("hex"),
            pubKey: account.pubKey.toString("hex"),
        }
    } catch(e) {
        console.error(e);
        return {
            privKey,
            pubKey: "",
        }
    }
};

const login = (privKey, pubKey) => {
    const account = verifyKeyPair(Buffer.from(privKey, 'hex'));
    account.privKey = account.privKey.toString("hex");
    account.pubKey = account.pubKey.toString('hex');
    if(account.pubKey !== pubKey) return {loggedIn: false};
    return {loggedIn: true}
};

const accountReducer = (state = {}, action) => {
    switch (action.type) {
        case types.ADD_NAME:
            return {...state, name: action.payload};
        case types.GENERATE_KEYPAIR:
            return {...state, ...createAccount()};
        case types.GENERATE_PUBKEY:
            return {...state, ...generatePubkey(action.payload)};
        case types.LOG_IN:
            if (!state.name || state.name === "") return state;
            if(!state.privKey || !state.pubKey) return state;
            return {...state, ...login(state.privKey, state.pubKey)};
        case types.LOG_OUT:
            return {};
        default:
            return state
    }
};

export default combineReducers({
    account: accountReducer,
});