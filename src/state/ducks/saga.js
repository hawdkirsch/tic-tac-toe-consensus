import { fork } from 'redux-saga/effects'
import gameSaga from './game/saga';
import tableSaga from './tables/saga';

export default function* saga(store) {
    yield fork(gameSaga, store);
    yield fork(tableSaga, store);
}