import * as types from './types';

const newGame = () => ({
    type: types.NEW_GAME
});

const gameover = () => ({
    type: types.GAMEOVER
});

const movePlayer = (player, cell) => ({
    type: types.MOVE,
    payload: { player, cell }
});

const switchPlayer = player => ({
    type: types.PLAYER,
    payload: player
});

const winner = player => ({
    type: types.WINNER,
    payload: player
});

const setBoard = (board) => ({
    type: types.SET_BOARD,
    payload: board,
});

const setSign = (sign) => ({
    type: types.SET_SIGN,
    payload: sign
})


export {
    newGame,
    gameover,
    movePlayer,
    switchPlayer,
    winner,

    setBoard,
    setSign
};