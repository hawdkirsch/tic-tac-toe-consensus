import {combineReducers} from "redux";

import * as types from "./types";
import * as actions from "./actions";
import GameState from "../../utils/game-state";

const emptyBoard = () => [
    0, 0, 0,
    0, 0, 0,
    0, 0, 0
];

const move = (board, {player, cell}) => {
    const updated = board;

    updated[cell] = player;
    return updated;
};

const clear = (board, cell) => {
    const updated = board;
    updated[cell] = 0;
    return updated;
}


const loadingReducer = (state = false, action) => {
    switch (action.type) {
        case "MAKE_MOVE":
        case "LOAD_BOARD_STATE":
            return true;
        case "LOAD_BOARD_STATE_SUCCESS":
        case "MAKE_MOVE_SUCCESS":
        case "MAKE_MOVE_FAIL":
            return false;
        default:
            return false;
    }
};

const boardReducer = (state = [], action) => {
    switch (action.type) {
        case types.NEW_GAME:
            return emptyBoard();
        case types.MOVE:
            return move(state, action.payload);
        case "RESET_MOVE":
            return clear(state, action.payload);
        case types.SET_BOARD:
            return action.payload;
        case types.SET_SIGN:
            return {...state};
        case "SET_GAME":
            return action.payload.board;
        default:
            return state;
    }
};
const gamePlayer = (state = {}, action) => {
    switch (action.type) {
        case types.SET_SIGN:
            return {...state, sign: action.payload};
        case "SET_GAME":
            const { whose_turn, players } = action.payload;
            return players.find(({ name }) => name === whose_turn).sign;
        default:
            return {...state};
    }

}

const gameoverReducer = (state = false, action) => {
    switch (action.type) {
        case types.NEW_GAME:
            return false;
        case types.GAMEOVER:
            return true;
        case types.WINNER:
            return true;
        case "SET_GAME":
            return action.payload.state === 2;
        default:
            return state;
    }
};

const winnerReducer = (state = -1, action) => {
    switch (action.type) {
        case types.WINNER:
            return action.payload;
        case types.NEW_GAME:
            return -1;
        default:
            return state;
    }
};

const playerReducer = (state = 1, action) => {
    switch (action.type) {
        case types.PLAYER:
            return action.payload;
        case types.NEW_GAME:
            return 1;
        default:
            return state;
    }
};

const gameReducer = (state = {board: emptyBoard()}, action) => {
    switch (action.type) {
        case "SET_GAME":
            return action.payload;
        case "CLEAR_GAME":
            return {board: emptyBoard()};
        default:
            return state;
    }
};

const boardLockReducer = (state = true, action) => {
    switch (action.type) {
        case "GAME_MOVE":
            return true;
        case "SET_GAME":
            const { isMyTurn } = action.payload;
            return !isMyTurn || action.payload.state !== GameState.Playing;
        default:
            return state;
    }
};

export default combineReducers({
    loading: loadingReducer,
    board: boardReducer,
    gameover: gameoverReducer,
    winner: winnerReducer,
    player: playerReducer,
    gamePlayer: gamePlayer,
    game: gameReducer,
    isBoardLocked: boardLockReducer
});
