import { newGame, gameover, switchPlayer, winner, movePlayer, setBoard, setSign } from "./actions";
import { isWinner, isDraw } from '../../utils/game';

const checkWinner = (board, player) => (dispatch) => {
    let hasWinner = true;

    if (isWinner(board, player)) {
        dispatch(winner(player));
        dispatch(gameover());
    } else if (isDraw(board)) {
        dispatch(winner(0));
        dispatch(gameover());
    } else {
        hasWinner = false;
    }

    return hasWinner;
};

const playTurn = (player, cell) => (dispatch) => {
    let nextPlayer;

    switch (player) {
        case 1:
            nextPlayer = 2;
            break;
        case 2:
            nextPlayer = 1;
            break;
        default:
            throw new Error("No user selected");
            break;
    }

    dispatch(movePlayer(player, cell));
    dispatch(switchPlayer(nextPlayer));
};

export {
    newGame,
    checkWinner,
    playTurn,
    setBoard,
    setSign
}