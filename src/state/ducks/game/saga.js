import { put, takeEvery, select } from 'redux-saga/effects';
import { makeMove, getGame } from "../../utils/blockchain";
import { isWinner, isDraw } from '../../utils/game';
import GameState from "../../utils/game-state";

function* sendMove(action) {
    try {
        const account = yield select(state => state.accountState.account);
        console.log(account);
        const playerName = yield select(state => state.accountState.account.name);
        const table = yield select(state => state.tablesState);
        console.log(table);
        const tableName = yield select(state => state.gameState.game.name);
        yield put({type: "GAME_MOVE", payload: action.payload});
        const board = yield select(state => state.gameState.board);
        if (isWinner(board, 1) || isWinner(board, 2)) {
            yield put({type: "GAME_WINNER", payload: 1});
            yield put({type: "GAME_OVER"});
        } else if (isDraw(board)) {
            yield put({type: "GAME_WINNER", payload: 0});
            yield put({type: "GAME_OVER"});
        }
        yield makeMove(account, playerName, tableName, action.payload.cell);
        yield put({type: "MAKE_MOVE_SUCCESS"})
        yield put({type: "LOAD_BOARD_STATE", payload: tableName})
    } catch (error) {
        yield put({type: "MAKE_MOVE_FAIL"})
        yield put({type: "RESET_MOVE", payload: action.payload.cell})
        console.error(error);
    }
}

function* loadBoardState(action) {
    const account = yield select(state => state.accountState.account);
    const game = yield getGame(action.payload);
    yield put({type: "SET_GAME", payload: {
        ...game,
        isMyTurn: game.whose_turn === account.name,
        mySign: (game.players.find(({ name }) => name === account.name) || {}).sign,
        winner: game.state === GameState.GameOver ? game.whose_turn : null,
    }});
    yield put({type: "LOAD_BOARD_STATE_SUCCESS"});
    if (isWinner(game.board, 1) || isWinner(game.board, 2)) {
        yield put({type: "GAME_WINNER", payload: 1});
        yield put({type: "GAME_OVER"});
    } else if (isDraw(game.board)) {
        yield put({type: "GAME_WINNER", payload: 0});
        yield put({type: "GAME_OVER"});
    }
}

export default function* gameSaga(store) {
    yield takeEvery("MAKE_MOVE", sendMove);
    yield takeEvery("LOAD_BOARD_STATE", loadBoardState);
}