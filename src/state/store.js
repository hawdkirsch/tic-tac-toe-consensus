import {createStore, combineReducers, applyMiddleware} from "redux";
import createSagaMiddleware from 'redux-saga';
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import thunk from 'redux-thunk';
import * as reducers from './ducks';
import saga from './ducks/saga';

const configureStore = (state = {},) => {
    const history = createBrowserHistory();
    const rootReducer = combineReducers({...reducers, router: connectRouter(history)});
    const sagaMiddleware = createSagaMiddleware();
    const middleware = [routerMiddleware(history), thunk, sagaMiddleware];
    const store = createStore(rootReducer, state, applyMiddleware(...middleware));
    sagaMiddleware.run(saga, store);
    return {
        ...store,
        history
    };
};
export {
    configureStore
}