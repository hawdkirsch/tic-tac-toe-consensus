import * as pcl from 'postchain-client';


const rest = pcl.restClient.createRestClient("http://localhost:7740/", '78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3', 5)
const gtx = pcl.gtxClient.createClient(
    rest,
    Buffer.from(
        '78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3',
        'hex'
    ),
    []
);

const keyPairToBuffer = (hexKeyPair) => ({pubKey: Buffer.from(hexKeyPair.pubKey, 'hex'), privKey: Buffer.from(hexKeyPair.privKey, 'hex')});

const makeKeyPair = () => pcl.util.makeKeyPair();
const verifyKeyPair = (privKey) => pcl.util.verifyKeyPair(privKey);

const registerPlayer = (hexKeyPair, name) => {
    const {pubKey, privKey} = keyPairToBuffer(hexKeyPair);
    const tx = gtx.newTransaction([pubKey]);
    tx.addOperation("create_player", name, pubKey);
    tx.sign(privKey, pubKey);
    return tx.postAndWaitConfirmation();
};

const newGame = (hexKeyPair, table_name, name) => {
    const {pubKey, privKey} = keyPairToBuffer(hexKeyPair);
    const tx = gtx.newTransaction([pubKey]);
    tx.addOperation("new_table", table_name, name);
    tx.sign(privKey, pubKey);
    return tx.postAndWaitConfirmation();
};

const addPlayer = (hexKeyPair, table_name, name) => {
    const  {pubKey, privKey} = keyPairToBuffer(hexKeyPair);
    const tx = gtx.newTransaction([pubKey]);
    tx.addOperation("add_player", name, table_name)
    tx.sign(privKey, pubKey);
    return tx.postAndWaitConfirmation();
};

const makeMove = (hexKeyPair, playerName, tableName, cell) => {
    const  {pubKey, privKey} = keyPairToBuffer(hexKeyPair);
    const tx = gtx.newTransaction([pubKey]);
    tx.addOperation("move", playerName, tableName, cell)
    tx.sign(privKey, pubKey);
    return tx.postAndWaitConfirmation();
};

const getPlayer = (name, pubKey) => {
    return gtx.query('getPlayer', {name, pubkey: pubKey});
};

const getPlayerTables = (pubkey) => {
    return gtx.query('getPlayerTables', {pubkey});
};

const getOpenTables = (pubkey) => {
    return gtx.query('getOpenTables', {pubkey});
};
const getBoard = (table_name) => {
    return gtx.query("getBoard", {table_name})
};

const getSign = (pubkey, table_name) => {
    return gtx.query("getSign", {pubkey, table_name});
};

const getGame = name => {
    return gtx.query("getGame", {name});
};

const getGames = () => {
    return gtx.query("getGames", {});
};

export {
    makeKeyPair,
    verifyKeyPair,

    registerPlayer,
    newGame,
    addPlayer,
    makeMove,

    getPlayer,
    getPlayerTables,
    getOpenTables,
    getBoard,
    getSign,
    getGame,
    getGames,
}