export default Object.freeze({
    Initial: 0,
    Playing: 1,
    Draw: 2,
    GameOver: 3,
});
