import '@babel/polyfill';
import React from 'react';
import {render} from 'react-dom';
import {Provider} from "react-redux";
import { ConnectedRouter } from "connected-react-router";

import App from './views/App.jsx';
import {configureStore} from "./state/store";


import {gameOperations} from './state/ducks/game';

let initialState = null;

//check localstorage
const rawStorage = localStorage.getItem('state');
if (rawStorage) {
    try {
        initialState = JSON.parse(rawStorage);
    } catch (e) {
        throw Error("State not readable");
    }
}

const store = configureStore(initialState || {});

if (!initialState) {
    const newGame = gameOperations.newGame();
    store.dispatch(newGame);
}

render (
    <Provider store={store}>
        <ConnectedRouter history={store.history}>
            <App state={initialState}/>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('app')
);